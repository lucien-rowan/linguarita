# Linguarita Changelog

## 0.0.2

- Added some more details to README.md
- Added CHANGELOG.md

## 0.0.1

- Added a basic function to translate applications in the first place.